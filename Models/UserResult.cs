using System;

namespace CucumberTechTest.Models
{
    public class UserResult {
        public string Name {get; set;}
        public string Amount {get; set;}
    }
}