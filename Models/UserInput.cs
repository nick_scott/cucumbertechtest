using System;

namespace CucumberTechTest.Models
{
    public class UserInput {
        public string Name {get; set;}
        public string Amount {get; set;}
    }
}