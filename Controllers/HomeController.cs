﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CucumberTechTest.Models;
using Humanizer;

namespace CucumberTechTest.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
                
        [HttpPost]
        public JsonResult StringifyInput([FromBody] UserInput amount) 
        {
            var dec = Decimal.Parse(amount?.Amount);
            //var dec = amount;
            var fract = ((int)(dec - Math.Truncate(dec))).ToWords();
            var whole = ((int)Math.Floor(dec)).ToWords();
            
            var res = new UserResult(){
        //        Name = input.Name,
                Amount = $"{whole} dollars and {fract} cents".ToUpper()
            };
            return Json(res);
        }
    }
}
